package com.JavanIntern.OddEven.controller;

import com.JavanIntern.OddEven.dto.OddEvenDto;
import com.JavanIntern.OddEven.response.OddEvenResponse;
import com.JavanIntern.OddEven.service.OddEvenService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("")
public class OddEvenController {

    private OddEvenService oddEvenService;

    @RequestMapping("")
    public String oddEven(Model model) {
        model.addAttribute("title", "Odd Even Web App");
        return "index";
    }

    @RequestMapping("/table")
    public String table(OddEvenDto oddEvenDto, Model model) {
        List<OddEvenResponse> oddEvenResponses = new ArrayList<OddEvenResponse>();

        oddEvenResponses = oddEvenService.getAllNumber(oddEvenDto.getNumber1(), oddEvenDto.getNumber2());

        model.addAttribute("title", "Odd Even Web App");
        model.addAttribute("allNumbers", oddEvenResponses);
        return "table";
    }
}
