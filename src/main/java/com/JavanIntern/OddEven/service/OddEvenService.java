package com.JavanIntern.OddEven.service;

import com.JavanIntern.OddEven.response.OddEvenResponse;

import java.util.ArrayList;
import java.util.List;

public class OddEvenService {

    public static List<OddEvenResponse> getAllNumber(int numberA, int numberB) {
        List<OddEvenResponse> oddEvenResponses = new ArrayList<OddEvenResponse>();

        for (int i = 0; i <= numberB - numberA; i++) {
            OddEvenResponse oddEvenResponse = new OddEvenResponse();

            int current = i + numberA;
            oddEvenResponse.setValue(current);
            if (current % 2 == 0) {
                oddEvenResponse.setDesc("genap");
            } else {
                oddEvenResponse.setDesc("ganjil");
            }

            oddEvenResponses.add(oddEvenResponse);
        }

        return oddEvenResponses;
    }
}
